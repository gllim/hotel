package cn.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Statement;
import java.util.ArrayList;

import cn.pojo.hotel_base;
import cn.pojo.hotel_user;

public class hotel_baseDao extends DBUtil {
	public boolean addHotel_Base(hotel_base base){     //增加酒店用户信息
		boolean flag = false;
		String sSql="insert into hotel_base values(?,?,?,?,?,?)";
		PreparedStatement pstmt = null;
		Connection conn = DBUtil.getConnection();
		try {
			if(conn == null)
				super.getConnection();
			pstmt=conn.prepareStatement(sSql);
			pstmt.setString(1, base.getHB_id());
			pstmt.setString(2, base.getHB_url());
			pstmt.setString(3, base.getHB_phone());
			pstmt.setString(4, base.getHB_exmail());
			pstmt.setString(5, base.getHB_profile());
			pstmt.setBytes(6, base.getHB_photo());
			int rs=pstmt.executeUpdate();
			if(rs !=0)
				flag = true;
			pstmt.close();
			conn.close();
			
		}catch (SQLException e) {

			e.printStackTrace();
		}

		return flag;
}
	
	public hotel_base queryHotel_Base(String ID) throws SQLException{    //通过ID获得酒店信息
		String sSql="select * from hotel_base where HB_id=?";
		PreparedStatement pstmt;
		pstmt = null;
		ResultSet rs = null;
		Connection conn = null;
		conn = DBUtil.getConnection();
			pstmt=conn.prepareStatement(sSql);
			pstmt.setString(1, ID);
			rs = pstmt.executeQuery();
			hotel_base hotel = new hotel_base();
			while(rs.next()) {
				hotel.setHB_id(rs.getString("HB_id"));
				hotel.setHB_url(rs.getString("HB_url"));
				hotel.setHB_phone(rs.getString("HB_phone"));
				hotel.setHB_exmail(rs.getString("HB_exmail"));
				hotel.setHB_profile(rs.getString("HB_profile"));
			    }
			if(rs!=null)
				rs.close();
			if(pstmt!=null)
				pstmt.close();
			if(conn!=null)
				conn.close();
		return hotel;
	}

	public boolean alterHotel_Base(hotel_base base)throws SQLException {     //通过ID修改酒店信息        
		boolean flag = false;
		String sSql="update hotel_base set HB_url=?,HB_phone=?,HB_exmail=?,HB_profile=?,HB_photo=? where HB_id=?";
		PreparedStatement pstmt;
		pstmt = null;
		Connection conn = null;
	    conn = DBUtil.getConnection();
	    pstmt = conn.prepareStatement(sSql);
		pstmt.setString(1, base.getHB_url());
		pstmt.setString(2, base.getHB_phone());
		pstmt.setString(3, base.getHB_exmail());
		pstmt.setString(4, base.getHB_profile());
		pstmt.setBytes(5, base.getHB_photo());
		pstmt.setString(6, base.getHB_id());
		int rs = pstmt.executeUpdate();
		if (rs != 0)
		flag = true;
		pstmt.close();
	    conn.close();
	    return flag;
		
	}
	public ArrayList<hotel_base> queryAll() throws SQLException{
		ArrayList<hotel_base> tt = new ArrayList<hotel_base>();
		String sSql = "select * from hotel_base";
		Statement stmt;
		Connection conn = DBUtil.getConnection();
		stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sSql);
		while(rs.next()) {
			hotel_base hotel = new hotel_base();
			hotel.setHB_id(rs.getString("HB_id"));
			hotel.setHB_url(rs.getString("HB_url"));
			hotel.setHB_phone(rs.getString("HB_phone"));
			hotel.setHB_exmail(rs.getString("HB_exmail"));
			hotel.setHB_profile(rs.getString("HB_profile"));
			tt.add(hotel);
		}
		rs.close();
		stmt.close();
		conn.close();
		return tt;
	}
	public boolean add(String ID) {
		boolean flag = false;
		String sSql="insert into hotel_base(HB_id) values(?)";
		PreparedStatement pstmt = null;
		Connection conn = DBUtil.getConnection();
		try {
			if(conn == null)
				super.getConnection();
			pstmt=conn.prepareStatement(sSql);
			pstmt.setString(1,ID);
			int rs=pstmt.executeUpdate();
			if(rs !=0)
				flag = true;
			pstmt.close();
			conn.close();
			
		}catch (SQLException e) {

			e.printStackTrace();
		}
		return flag;
	}
	
	public ArrayList<hotel_base> allHotel_Base() throws SQLException{     //获得全部酒店的信息
		ArrayList<hotel_base> aa = new ArrayList<hotel_base>();
		Connection conn = DBUtil.getConnection();
		String sql = "select * from hotel_base;";
		PreparedStatement pstmt = conn.prepareStatement(sql);
		ResultSet rs = pstmt.executeQuery();
		while(rs.next()) {
			hotel_base qq = new hotel_base();
			qq.setHB_id(rs.getString("HB_id"));
			qq.setHB_url(rs.getString("HB_url"));
			aa.add(qq);
		}
		rs.close();
		pstmt.close();
		conn.close();
		return aa;	
	}
	public ArrayList<hotel_base> likeHotel_Base(String hotelname) throws SQLException{     //根据酒店名模糊查询酒店
		ArrayList<hotel_base> aa = new ArrayList<hotel_base>();
		Connection conn = DBUtil.getConnection();
		String sql = "select * from hotel_base where HB_id like '%" + hotelname + "%'";
		PreparedStatement pstmt = conn.prepareStatement(sql);
		ResultSet rs = pstmt.executeQuery();
		while(rs.next()) {
			hotel_base qq = new hotel_base();
			qq.setHB_id(rs.getString("HB_id"));
			qq.setHB_url(rs.getString("HB_url"));
			aa.add(qq);
		}
		rs.close();
		pstmt.close();
		conn.close();
		return aa;
		
	}
}
