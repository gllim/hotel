package cn.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import cn.pojo.hotel_user;

public class hotel_userDao {
	public boolean addHotel_User(hotel_user qq) throws SQLException {//添加酒店用户
		boolean aa =false;int rs = 0;
		Connection conn = DBUtil.getConnection();
		String sql = "insert into hotel_user values(?,?,'待审核')";
		PreparedStatement pstmt = conn.prepareStatement(sql);
		pstmt.setString(1,qq.getHU_Id());
		pstmt.setString(2, qq.getHU_Passwd());
		rs = pstmt.executeUpdate();
		if(rs == 1)aa = true;
		pstmt.close();
		conn.close();
		return aa;
	}
	
	public boolean alterHotel_User(String ID,String statuse) throws SQLException {//修改酒店用户状态
		boolean aa = false; int rs = 0;
		Connection conn = DBUtil.getConnection();
		String sql = "update hotel_user set HU_statuse = ? where HU_Id = ?";
		PreparedStatement pstmt = conn.prepareStatement(sql);
		pstmt.setString(1, statuse);
		pstmt.setString(2, ID);
		rs = pstmt.executeUpdate();
		if(rs == 1)aa = true;
		pstmt.close();
		conn.close();
		return aa;
	}
	
	public ArrayList<hotel_user> allHotel_User() throws SQLException{//获得全部酒店用户
		ArrayList<hotel_user> aa = new ArrayList<hotel_user>();
		Connection conn = DBUtil.getConnection();
		String sql = "select * from hotel_user;";
		PreparedStatement pstmt = conn.prepareStatement(sql);
		ResultSet rs = pstmt.executeQuery();
		while(rs.next()) {
			hotel_user qq = new hotel_user();
			qq.setHU_Id(rs.getString("HU_Id"));
			qq.setHU_statuse(rs.getString("HU_statuse"));
			aa.add(qq);
		}
		rs.close();
		pstmt.close();
		conn.close();
		return aa;
		
	}
     public	boolean ReSHotel_User(String ID ,String Passwd) throws SQLException {//判断账号密码一致并获得状态

        Connection conn=null;PreparedStatement pstmt=null;ResultSet rs; boolean result=false;
		conn = DBUtil.getConnection();
		String Sql = "SELECT * FROM hotel_user where HU_Id = ? and HU_Passwd=?";
		pstmt = conn.prepareStatement(Sql);
		pstmt.setString(1,ID);
		pstmt.setString(2,Passwd);
		rs = pstmt.executeQuery();
		while(rs.next()) {
			if(rs.getString("HU_statuse").equals("通过"))result = true;
		}
		pstmt.close();
		conn.close();
		return result;
   }

   public ArrayList<hotel_user> allqqq() throws SQLException{//获得全部酒店用户
		ArrayList<hotel_user> aa = new ArrayList<hotel_user>();
		Connection conn = DBUtil.getConnection();
		String sql = "select * from hotel_user;";
		PreparedStatement pstmt = conn.prepareStatement(sql);
		ResultSet rs = pstmt.executeQuery();
		while(rs.next()) {
			hotel_user qq = new hotel_user();
			qq.setHU_Id(rs.getString("HU_Id"));
			qq.setHU_Passwd(rs.getString("HU_passwd"));
			qq.setHU_statuse(rs.getString("HU_statuse"));
			aa.add(qq);
		}
		rs.close();
		pstmt.close();
		conn.close();
		return aa;
	}
   public hotel_user querystatuse(String ID) throws SQLException {
	   hotel_user aa = new hotel_user();
	   Connection conn = DBUtil.getConnection();
	   String sql = "SELECT * FROM hotel_user where HU_Id = ?";
	   PreparedStatement pstmt = conn.prepareStatement(sql);
	   pstmt.setString(1, ID);
	   ResultSet rs = pstmt.executeQuery();
	   while(rs.next()) {
		   aa.setHU_Id(rs.getString("HU_Id"));
		   aa.setHU_statuse(rs.getString("HU_statuse"));
	   }
	   rs.close();
	   pstmt.close();
	   conn.close();
	return aa;
	   
   }
 }


