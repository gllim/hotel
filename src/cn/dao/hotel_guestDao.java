package cn.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import cn.pojo.hotel_guest;


public class hotel_guestDao extends DBUtil {

	public boolean addHotel_Guest(hotel_guest guest) throws SQLException { // 增加个人用户信息
		boolean flag = false;
		String sSql = "insert into hotel_guest values (?,?,?,?,?,?)";
		Connection conn= DBUtil.getConnection();
		PreparedStatement pstmt ;
		pstmt = conn.prepareStatement(sSql);
		pstmt.setString(1, guest.getHG_name());
		pstmt.setString(2, guest.getHG_sex());
		pstmt.setString(3, guest.getHG_passwd());
		pstmt.setString(4, guest.getHG_phone());
		pstmt.setString(5, guest.getHG_exmail());
		pstmt.setString(6, guest.getHG_idcard());
		int count = pstmt.executeUpdate();
		if (count!= 0)
			flag = true;
		pstmt.close();
		conn.close();
		return flag;

	}


	
	public hotel_guest queryHotel_Guest(String phone) { // 通过phone来获得个人信息
		hotel_guest guest_phone = null;
		Connection conn = DBUtil.getConnection();
		String sSql = "select * from hotel_guest where HG_phone=?";
		PreparedStatement pstmt = null;
		try {
			pstmt = conn.prepareStatement(sSql);
			pstmt.setString(1, phone);
			ArrayList<hotel_guest> list=new ArrayList<hotel_guest>();
			ResultSet rs = pstmt.executeQuery();
			if (rs != null && rs.next()) {
				guest_phone = new hotel_guest();
				guest_phone.setHG_idcard(rs.getString("HG_idcard"));
				guest_phone.setHG_passwd(rs.getString("HG_passwd"));
				guest_phone.setHG_name(rs.getString("HG_name"));
				guest_phone.setHG_phone(rs.getString("HG_phone"));
				guest_phone.setHG_exmail(rs.getString("HG_exmail"));
				guest_phone.setHG_sex(rs.getString("HG_sex"));
				list.add(guest_phone);
			}
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return guest_phone;
	}

	public boolean alterHoel_Guest(hotel_guest phone) throws SQLException { // 根据phone修改个人信息
		boolean flag = false;
		String sSql = "update hotel_guest set HG_name=?,HG_sex=?,HG_passwd=?,HG_exmail=?,HG_idcard=? where HG_phone=?";
		PreparedStatement pstmt;
		Connection conn = DBUtil.getConnection();
		pstmt = conn.prepareStatement(sSql);
		pstmt.setString(1, phone.getHG_name());
		pstmt.setString(2, phone.getHG_sex());
		pstmt.setString(3, phone.getHG_passwd());
		pstmt.setString(4, phone.getHG_exmail());
		pstmt.setString(5, phone.getHG_idcard());
		pstmt.setString(6, phone.getHG_phone());
		int rs = pstmt.executeUpdate();
		if (rs != 0)
			flag = true;
		pstmt.close();
		conn.close();
		return flag;

	}

	public ArrayList<hotel_guest> queryallhotle_guest() { // 查找全部用户信息
		ArrayList<hotel_guest> list = null;
		String sSql = "select * from hotel_guest";
		PreparedStatement pstmt;
		Connection conn = DBUtil.getConnection();
		try {
			pstmt = conn.prepareStatement(sSql);
			ResultSet rs = pstmt.executeQuery();
			if (rs != null) {
				list = new ArrayList<hotel_guest>();
				while (rs.next()) {
					hotel_guest hg = new hotel_guest();
					hg.setHG_idcard(rs.getString("HG_idcard"));
					hg.setHG_passwd(rs.getString("HG_passwd"));
					hg.setHG_name(rs.getString("HG_name"));
					hg.setHG_sex(rs.getString("HG_sex"));
					hg.setHG_exmail(rs.getString("HG_exmail"));
					hg.setHG_phone(rs.getString("HG_phone"));
					list.add(hg);
				}
			}
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	public boolean dohotel_guest(String hg_phone, String hg_passwd) throws SQLException{ // 根据账号密码查找用户
		boolean aa=false;
		String sql = "select * from hotel_guest where HG_phone=? and HG_passwd=?";
		Connection conn = DBUtil.getConnection();
		PreparedStatement pstmt = null;
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, hg_phone);
			pstmt.setString(2, hg_passwd);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				if(hg_phone.equals(rs.getString("HG_phone")))aa = true;
			}
			rs.close();
			pstmt.close();
			conn.close();
			return aa;
		
	}
	
	public boolean UemlHoel_Guest(String phone,String passwd) throws SQLException { // 根据手机号修改密码
		Connection conn = null;PreparedStatement pstmt=null;int rs =0;
		boolean flag = false;
		conn = DBUtil.getConnection();
		String sSql = "update hotel_guest set HG_passwd=? where HG_phone=?";
		pstmt = conn.prepareStatement(sSql);
		pstmt.setString(1, passwd);
		pstmt.setString(2, phone);
		rs= pstmt.executeUpdate();
		if (rs!= 0)flag = true;
		else flag=false;
		pstmt.close();
		conn.close();
		return flag;
	}
	public boolean ExmaillHoel_Guest(String phone,String exmail) throws SQLException { // 根据手机号修改邮箱
		Connection conn = null;PreparedStatement pstmt=null;int rs =0;
		boolean flag = false;
		conn = DBUtil.getConnection();
		String sSql = "update hotel_guest set HG_exmail=? where HG_phone=?";
		pstmt = conn.prepareStatement(sSql);
		pstmt.setString(1, exmail);
		pstmt.setString(2, phone);
		rs= pstmt.executeUpdate();
		if (rs!= 0)flag = true;
		else flag=false;
		pstmt.close();
		conn.close();
		return flag;
	}
	public boolean ppHotel_Guest (hotel_guest HG_passwd) throws SQLException{         //判断密码是否相同
		boolean flag=false;
		String sql="selct phone from hotel_guest where HG_passwd = '"+HG_passwd+"'";
		PreparedStatement pstmt;
		Connection conn = DBUtil.getConnection();
		pstmt = conn.prepareStatement(sql);
		pstmt.setString(1, HG_passwd.getHG_name());
		pstmt.setString(2, HG_passwd.getHG_sex());
		pstmt.setString(3, HG_passwd.getHG_passwd());
		pstmt.setString(4, HG_passwd.getHG_phone());
		pstmt.setString(5, HG_passwd.getHG_idcard());
		pstmt.setString(6, HG_passwd.getHG_phone());
		int rs = pstmt.executeUpdate();
		if (rs != 0)
			flag = true;
		pstmt.close();
		conn.close();
		return flag;	
	}
	public boolean deleteguest(String phone) throws SQLException {              //通过手机号删除
		Connection conn=null;PreparedStatement pstmt=null;int rs=0;boolean aa=false;
		conn=DBUtil.getConnection();
		String sql="delete from hotel_guest where HG_phone=?";
		pstmt=conn.prepareStatement(sql);
		pstmt.setString(1, phone);
		rs = pstmt.executeUpdate();
		if(rs==0)aa=false;
		else aa=true;
		if(pstmt!=null)pstmt.close();	
		if(conn!=null)conn.close();
		return aa;
	}

}	