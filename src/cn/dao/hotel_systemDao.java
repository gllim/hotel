package cn.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class hotel_systemDao {
	public boolean judgeSystem(String ID,String passwd) throws SQLException {//�ж��˺�
		boolean result=false;
		Connection conn = DBUtil.getConnection();
		String Sql = "SELECT * FROM hotel_system where HS_id = ?";
		PreparedStatement pstmt = conn.prepareStatement(Sql);
		pstmt.setString(1,ID);
		ResultSet rs = pstmt.executeQuery();
		while(rs.next()) {
			if(passwd.equals(rs.getString("HS_passwd")))result = true;
		}
		rs.close();
		pstmt.close();
		conn.close();
		return result;
	}
}
