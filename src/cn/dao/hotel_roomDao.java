package cn.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import cn.pojo.hotel_room;
import cn.dao.DBUtil;
public class hotel_roomDao extends DBUtil{
	public boolean addHotel_Room(hotel_room room) throws SQLException {             //增加房间
		Connection conn=null;PreparedStatement pstmt=null;int rs=0;boolean aa=false;
		conn=DBUtil.getConnection();
		String sql="insert into hotel_room values(?,?,?,?,?,?,?,?,?,?)";
		pstmt=conn.prepareStatement(sql);
		pstmt.setInt(1,room.getHR_id());
		pstmt.setString(2, room.getHR_name());
		pstmt.setInt(3, room.getHR_area());
		pstmt.setString(4, room.getHR_bedType());
		pstmt.setInt(5, room.getHR_checkinNumber());
		pstmt.setInt(6, room.getHR_floor());
		pstmt.setString(7, room.getHR_window());
		pstmt.setDouble(8, room.getHR_prise());
		pstmt.setBytes(9, room.getHR_photo());
		pstmt.setString(10, room.getHR_hotelname());
		rs=pstmt.executeUpdate();
		if(rs==0)aa=false;
		else aa=true;
		pstmt.close();
		conn.close();
		return aa;
	}
	public hotel_room queryHotel_Room(int ID) throws SQLException {          //通过房间ID查找房间
		Connection conn=null;PreparedStatement pstmt=null;ResultSet rs=null;
		conn=DBUtil.getConnection();
		String sql="select * from hotel_room where HR_id =?";
		pstmt=conn.prepareStatement(sql);
		pstmt.setInt(1, ID);
		rs = pstmt.executeQuery();
		hotel_room room=new hotel_room();
		while(rs.next()){
			room.setHR_id(rs.getInt("HR_id"));
			room.setHR_name(rs.getString("HR_name"));
			room.setHR_area(rs.getInt("HR_area"));
			room.setHR_bedType(rs.getString("HR_bedType"));
			room.setHR_checkinNumber(rs.getInt("HR_checkinNumber"));
			room.setHR_floor(rs.getInt("HR_floor"));
			room.setHR_window(rs.getString("HR_window"));
			room.setHR_prise(rs.getDouble("HR_prise"));
			room.setHR_photo(rs.getBytes("HR_photo"));
			room.setHR_hotelname(rs.getString("HR_hotelname"));	
		}
		if(rs!=null)rs.close();
		if(pstmt!=null)pstmt.close();	
		if(conn!=null)conn.close();
		return room;
	}
	public boolean alterHotel_Room(hotel_room room) throws SQLException { //修改房间信息
		Connection conn=null;PreparedStatement pstmt=null;int rs=0;boolean aa=false;
		conn=DBUtil.getConnection();
		String sql="update hotel_room set HR_name=?,HR_area=?,HR_bedType=?,HR_checkinNumber=?,HR_floor=?,HR_window=?,HR_prise=?,HR_photo=? where HR_id=?";
		pstmt=conn.prepareStatement(sql);
		pstmt.setString(1, room.getHR_name());
		pstmt.setInt(2, room.getHR_area());
		pstmt.setString(3, room.getHR_bedType());
		pstmt.setInt(4, room.getHR_checkinNumber());
		pstmt.setInt(5, room.getHR_floor());
		pstmt.setString(6, room.getHR_window());
		pstmt.setDouble(7, room.getHR_prise());
		pstmt.setBytes(8, room.getHR_photo());
		pstmt.setInt(9,room.getHR_id());
		rs = pstmt.executeUpdate();
		if(rs==0)aa=false;
		else aa=true;
		pstmt.close();
		conn.close();		
		return aa;
	}
	public ArrayList<hotel_room> queryAllRoom(String hotel) throws SQLException{      //根据酒店名查全部房间
		Connection conn=null;PreparedStatement pstmt=null;ResultSet rs=null;
		conn=DBUtil.getConnection();
		String sql="select * from hotel_room where HR_hotelname=?";
		pstmt=conn.prepareStatement(sql);
		pstmt.setString(1, hotel);
		ArrayList<hotel_room> list =new ArrayList<hotel_room>();
		rs = pstmt.executeQuery();
		while(rs.next()){
			hotel_room room=new hotel_room();
			room.setHR_id(rs.getInt("HR_id"));
			room.setHR_name(rs.getString("HR_name"));
			room.setHR_area(rs.getInt("HR_area"));
			room.setHR_bedType(rs.getString("HR_bedType"));
			room.setHR_checkinNumber(rs.getInt("HR_checkinNumber"));
			room.setHR_floor(rs.getInt("HR_floor"));
			room.setHR_window(rs.getString("HR_window"));
			room.setHR_prise(rs.getDouble("HR_prise"));
			room.setHR_photo(rs.getBytes("HR_photo"));
			room.setHR_hotelname(rs.getString("HR_hotelname"));	
			list.add(room);
		}
		if(rs!=null)rs.close();
		if(pstmt!=null)pstmt.close();	
		if(conn!=null)conn.close();
		return list;
	}
	public boolean deleteRoom(int ID) throws SQLException {              //通过房间ID删除房间
		Connection conn=null;PreparedStatement pstmt=null;int rs=0;boolean aa=false;
		conn=DBUtil.getConnection();
		String sql="delete from hotel_room where HR_id=?";
		pstmt=conn.prepareStatement(sql);
		pstmt.setInt(1, ID);
		rs = pstmt.executeUpdate();
		if(rs==0)aa=false;
		else aa=true;
		if(pstmt!=null)pstmt.close();	
		if(conn!=null)conn.close();
		return aa;
	}
	public hotel_room queryHotel_Roomphoto(int ID) throws SQLException {          //通过房间ID查找房间除了照片
		Connection conn=null;PreparedStatement pstmt=null;ResultSet rs=null;
		conn=DBUtil.getConnection();
		String sql="select * from hotel_room where HR_id =?";
		pstmt=conn.prepareStatement(sql);
		pstmt.setInt(1, ID);
		rs = pstmt.executeQuery();
		hotel_room room=new hotel_room();
		while(rs.next()){
			room.setHR_id(rs.getInt("HR_id"));
			room.setHR_name(rs.getString("HR_name"));
			room.setHR_area(rs.getInt("HR_area"));
			room.setHR_bedType(rs.getString("HR_bedType"));
			room.setHR_checkinNumber(rs.getInt("HR_checkinNumber"));
			room.setHR_floor(rs.getInt("HR_floor"));
			room.setHR_window(rs.getString("HR_window"));
			room.setHR_prise(rs.getDouble("HR_prise"));
			room.setHR_hotelname(rs.getString("HR_hotelname"));	
		}
		if(rs!=null)rs.close();
		if(pstmt!=null)pstmt.close();	
		if(conn!=null)conn.close();
		return room;
	}


public ArrayList<hotel_room> likeHotel_room(String  roomname) throws SQLException{
	ArrayList<hotel_room> aroom = new ArrayList<hotel_room>();
	Connection conn = DBUtil.getConnection();
	String sql = "select * from hotel_room where HR_id like '%" + roomname + "%'";
	PreparedStatement pstmt = conn.prepareStatement(sql);
	ResultSet rs = pstmt.executeQuery();
	while(rs.next()) {
		hotel_room room = new hotel_room();
		room.setHR_id(rs.getInt("HR_id"));
		room.setHR_name(rs.getString("HR_name"));
		room.setHR_area(rs.getInt("HR_area"));
		room.setHR_bedType(rs.getString("HR_bedType"));
		room.setHR_checkinNumber(rs.getInt("HR_checkinNumber"));
		room.setHR_floor(rs.getInt("HR_floor"));
		room.setHR_window(rs.getString("HR_window"));
		room.setHR_prise(rs.getDouble("HR_prise"));
		room.setHR_hotelname(rs.getString("HR_hotelname"));	
		aroom.add(room);
	}
	rs.close();
	pstmt.close();
	conn.close();
	return aroom;
	
	
}
}
