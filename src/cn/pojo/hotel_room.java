package cn.pojo;

public class hotel_room {
	private int HR_id;
	private String HR_name;
	private int HR_area;
	private String HR_bedType;
	private int HR_checkinNumber;
	private int HR_floor;
	private String HR_window;
	private double HR_prise;
	private byte[] HR_photo;
	private String HR_hotelname;
	public int getHR_id() {
		return HR_id;
	}
	public void setHR_id(int hR_id) {
		HR_id = hR_id;
	}
	public String getHR_name() {
		return HR_name;
	}
	public void setHR_name(String hR_name) {
		HR_name = hR_name;
	}
	public int getHR_area() {
		return HR_area;
	}
	public void setHR_area(int hR_area) {
		HR_area = hR_area;
	}
	public String getHR_bedType() {
		return HR_bedType;
	}
	public void setHR_bedType(String hR_bedType) {
		HR_bedType = hR_bedType;
	}
	public int getHR_checkinNumber() {
		return HR_checkinNumber;
	}
	public void setHR_checkinNumber(int hR_checkinNumber) {
		HR_checkinNumber = hR_checkinNumber;
	}
	public int getHR_floor() {
		return HR_floor;
	}
	public void setHR_floor(int hR_floor) {
		HR_floor = hR_floor;
	}
	public String getHR_window() {
		return HR_window;
	}
	public void setHR_window(String hR_window) {
		HR_window = hR_window;
	}
	public double getHR_prise() {
		return HR_prise;
	}
	public void setHR_prise(double hR_prise) {
		HR_prise = hR_prise;
	}
	public byte[] getHR_photo() {
		return HR_photo;
	}
	public void setHR_photo(byte[] hR_photo) {
		HR_photo = hR_photo;
	}
	public String getHR_hotelname() {
		return HR_hotelname;
	}
	public void setHR_hotelname(String hR_hotelname) {
		HR_hotelname = hR_hotelname;
	}
	
	
	
}
