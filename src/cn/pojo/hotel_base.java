package cn.pojo;

import java.sql.Blob;

public class hotel_base {
	private String HB_id;
	private String HB_url;
	private String HB_phone;
	private String HB_exmail;
	private String HB_profile;
	private byte[] HB_photo;
	public String getHB_id() {
		return HB_id;
	}
	public void setHB_id(String hB_id) {
		HB_id = hB_id;
	}
	public String getHB_url() {
		return HB_url;
	}
	public void setHB_url(String hB_url) {
		HB_url = hB_url;
	}
	public String getHB_phone() {
		return HB_phone;
	}
	public void setHB_phone(String hB_phone) {
		HB_phone = hB_phone;
	}
	public String getHB_exmail() {
		return HB_exmail;
	}
	public void setHB_exmail(String hB_exmail) {
		HB_exmail = hB_exmail;
	}
	public String getHB_profile() {
		return HB_profile;
	}
	public void setHB_profile(String hB_profile) {
		HB_profile = hB_profile;
	}
	public byte[] getHB_photo() {
		return HB_photo;
	}
	public void setHB_photo(byte[] hB_photo) {
		HB_photo = hB_photo;
	}
	
	
	
}
