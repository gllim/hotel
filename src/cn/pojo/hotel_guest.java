package cn.pojo;

public class hotel_guest {
	private String HG_name;
	private String HG_sex;
	private String HG_passwd;
	private String HG_phone;
	private String HG_exmail;
	private String HG_idcard;
	public String getHG_name() {
		return HG_name;
	}
	public void setHG_name(String hG_name) {
		HG_name = hG_name;
	}
	public String getHG_sex() {
		return HG_sex;
	}
	public void setHG_sex(String hG_sex) {
		HG_sex = hG_sex;
	}
	public String getHG_passwd() {
		return HG_passwd;
	}
	public void setHG_passwd(String hG_passwd) {
		HG_passwd = hG_passwd;
	}
	public String getHG_phone() {
		return HG_phone;
	}
	public void setHG_phone(String hG_phone) {
		HG_phone = hG_phone;
	}
	public String getHG_exmail() {
		return HG_exmail;
	}
	public void setHG_exmail(String hG_exmail) {
		HG_exmail = hG_exmail;
	}
	public String getHG_idcard() {
		return HG_idcard;
	}
	public void setHG_idcard(String hG_idcard) {
		HG_idcard = hG_idcard;
	}

}
