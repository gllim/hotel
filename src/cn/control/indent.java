package cn.control;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.dao.hotel_baseDao;
import cn.dao.hotel_roomDao;
import cn.pojo.hotel_base;
import cn.pojo.hotel_room;

@SuppressWarnings("serial")
public class indent extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int room_id = Integer.parseInt(req.getParameter("RoomID"));
		String hotelName = req.getParameter("hotelName");
		hotel_room room = new hotel_room();hotel_base jiudian = new hotel_base();
		hotel_roomDao text1 = new hotel_roomDao(); hotel_baseDao text2 = new hotel_baseDao();
		try {
			room = text1.queryHotel_Roomphoto(room_id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			jiudian = text2.queryHotel_Base(hotelName);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		Calendar calendar = Calendar.getInstance();
		String dateName = df.format(calendar.getTime());
		req.setAttribute("room", room);
		req.setAttribute("hotel", jiudian);
		req.setAttribute("HI_indentNumber", dateName);
		RequestDispatcher rs = req.getRequestDispatcher("jsp/BookJS.jsp");
		rs.forward(req, resp);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
}
