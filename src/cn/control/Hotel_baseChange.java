package cn.control;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import cn.dao.hotel_baseDao;
import cn.pojo.hotel_base;

@SuppressWarnings("serial")
public class Hotel_baseChange extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<String> qqq=new ArrayList<String>();
		List<FileItem> Files = new ArrayList<FileItem>();//存取上传文件 
	    DiskFileItemFactory fu = new DiskFileItemFactory ();//创建一个解析器工厂
	    ServletFileUpload upload = new ServletFileUpload(fu);
		upload.setHeaderEncoding("utf-8");
		byte[] buffer = null;
		try {
			List<FileItem> list = upload.parseRequest(req);//取得表单的数据内容
			for(FileItem items:list){
				if(items.isFormField()){
					qqq.add(items.getString("utf-8"));
				}else {
					Files.add(items);
				}
			}
		for(int i=0;i<Files.size();i++){
			FileItem item = (FileItem)Files.get(i);
			InputStream file = item.getInputStream(); 
			buffer = new byte[file.available()];
			file.read(buffer);
			}
		}
		catch (FileUploadException e1) {
			System.out.println(e1);
		}
		hotel_base base = new hotel_base();
		base.setHB_id(qqq.get(0));
		base.setHB_phone(qqq.get(1));
		base.setHB_exmail(qqq.get(2));
		base.setHB_url(qqq.get(3));
		base.setHB_profile(qqq.get(4));
		base.setHB_photo(buffer);
		hotel_baseDao baseDao = new hotel_baseDao();
		HttpSession session = req.getSession();
		String id=(String) session.getAttribute("HU_Id");
		boolean alter = false;
		try {
			alter = baseDao.alterHotel_Base(base);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		if(alter == true) {
			req.setAttribute("base", base);
			RequestDispatcher rd = req.getRequestDispatcher("jsp/hotel/hotel_message.jsp");
		}
		
		resp.sendRedirect("Hotel_main");	
			
}
@Override
protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	// TODO Auto-generated method stub
	doGet(req, resp);
}
}
