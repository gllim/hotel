package cn.control;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.dao.hotel_indentDao;
import cn.pojo.hotel_indent;

@SuppressWarnings("serial")
public class indentAdd extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		 req.setCharacterEncoding("utf-8");
		 String HI_indentNumber = req.getParameter("HI_indentNumber");
		 int HI_roomId = Integer.parseInt(req.getParameter("HI_roomId"));
		 String HI_hotelName = req.getParameter("HI_hotelName");
		 String HI_userPhone = req.getParameter("HI_userPhone");
		 
		 String courseDate1 = req.getParameter("HI_checkinDate");
		 SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 java.util.Date date = null;
		 try {
			date = formatter.parse(courseDate1);
		 } catch (ParseException e) {
			e.printStackTrace();
		 }
		 Timestamp HI_checkinDate = new Timestamp(date.getTime());
		 
		 String courseDate2 = req.getParameter("HI_departureDate");
		 SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 java.util.Date date1 = null;
		 try {
			date1 = formatter1.parse(courseDate2);
		 } catch (ParseException e) {
			e.printStackTrace();
		 }
		 Timestamp HI_departureDate = new Timestamp(date1.getTime());
		 
		 String HI_guestName = req.getParameter("HI_guestName");
		 String HI_guestPhone = req.getParameter("HI_guestPhone");
		 String HI_guestIdCard = req.getParameter("HI_guestIdCard");
		 String statuse = "正在预订";
		 Double HI_prise = Double.parseDouble(req.getParameter("HI_prise"));
		 hotel_indent ww = new hotel_indent();
		 ww.setHI_checkinDate(HI_checkinDate);
		 ww.setHI_departureDate(HI_departureDate);
		 ww.setHI_guestIdCard(HI_guestIdCard);
		 ww.setHI_guestName(HI_guestName);
		 ww.setHI_guestPhone(HI_guestPhone);
		 ww.setHI_hotelName(HI_hotelName);
		 ww.setHI_indentNumber(HI_indentNumber);
		 ww.setHI_prise(HI_prise);
		 ww.setHI_roomId(HI_roomId);
		 ww.setHI_userPhone(HI_userPhone);
		 ww.setHI_statuse(statuse);
		 hotel_indentDao text = new hotel_indentDao();boolean text1 = false;
		 try {
			text1 = text.addHotel_Indent(ww);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(text1 == true) {
			req.setAttribute("HI_prise", HI_prise);
			RequestDispatcher rd = req.getRequestDispatcher("jsp/guest/pay.jsp");
			rd.forward(req, resp);
		}else {
			System.out.println("订单出现问题");
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
}
