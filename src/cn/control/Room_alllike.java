package cn.control;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.dao.hotel_roomDao;
import cn.pojo.hotel_room;

public class Room_alllike extends HttpServlet{
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String roomname = req.getParameter("roomname");
		ArrayList<hotel_room> list = new ArrayList<hotel_room>();
		hotel_room room = new hotel_room();
		hotel_roomDao roomdao = new hotel_roomDao();
		try {
			HttpSession session = req.getSession();
			session.setAttribute("roomname", roomname);
			list = roomdao.likeHotel_room(roomname);
			req.setAttribute("roomAll", list);
			RequestDispatcher rd=req.getRequestDispatcher("jsp/Room.jsp");
	    	rd.forward(req, resp);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
}
