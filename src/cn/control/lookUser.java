package cn.control;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.dao.hotel_baseDao;
import cn.dao.hotel_guestDao;
import cn.dao.hotel_userDao;
import cn.pojo.hotel_base;
import cn.pojo.hotel_guest;
import cn.pojo.hotel_user;

@SuppressWarnings("serial")
public class lookUser extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		 String type = req.getParameter("type");
		 
		 if(type.equals("hotel_user")) {
			 hotel_userDao ww = new hotel_userDao();
			 ArrayList<hotel_user> aa = new ArrayList<hotel_user>();
			 try {
				aa = ww.allHotel_User();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 req.setAttribute("hotel_user", aa);
			 RequestDispatcher rd = req.getRequestDispatcher("jsp/system/modify_hotel_Status.jsp");
			 rd.forward(req, resp);
		 }
		 
		 if(type.equals("hotel_guest")) {
			 hotel_guestDao ww = new hotel_guestDao();
			 ArrayList<hotel_guest> list = new ArrayList<hotel_guest>();
			 list = ww.queryallhotle_guest();
			 req.setAttribute("hotel_guest", list);
			 RequestDispatcher rd = req.getRequestDispatcher("jsp/system/individual_Users.jsp");
			 rd.forward(req, resp);
		 }
		 
		 if(type.equals("hotel_base")) {
			 hotel_userDao aa = new hotel_userDao();
			 ArrayList<hotel_user> list = new ArrayList<hotel_user>();
			 try {
				list = aa.allqqq();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 req.setAttribute("hotel_base",list);
			 RequestDispatcher rd = req.getRequestDispatcher("jsp/system/hotel_Users.jsp");
			 rd.forward(req, resp);
			 
		 }
	}
}
