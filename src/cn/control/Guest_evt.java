package cn.control;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.dao.hotel_indentDao;
import cn.pojo.hotel_indent;

public class Guest_evt extends HttpServlet{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String phone =req.getParameter("phone");
		hotel_indentDao indentdao = new hotel_indentDao();
	    ArrayList<hotel_indent> list = new ArrayList<hotel_indent>();
		try {
			list = indentdao.queryHotel_Indent_guestPhone(phone);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		req.setAttribute("list", list);
		RequestDispatcher rd=req.getRequestDispatcher("jsp/guest/hotel_guest_right.jsp");
		rd.forward(req, resp);	
		
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
}
