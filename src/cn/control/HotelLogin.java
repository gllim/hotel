package cn.control;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.dao.hotel_guestDao;
import cn.dao.hotel_userDao;
import cn.pojo.hotel_guest;


public class HotelLogin extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    boolean hu = false;
	    request.setCharacterEncoding("utf-8");
		String Id=request.getParameter("HG_phone");							
		String Passwd=request.getParameter("HG_passwd");
	    hotel_userDao HU=new hotel_userDao();
	    try {
			hu= HU.ReSHotel_User(Id ,Passwd);
			if(hu == true) {
		        HttpSession se = request.getSession();
		        se.setAttribute("HU_Id",Id);
		        response.sendRedirect("jsp/hotel/hotel_main.jsp");
		    }
			else {
				boolean hg = false;
				hotel_guestDao HG=new hotel_guestDao();
				hg= HG.dohotel_guest(Id, Passwd);
				hotel_guest guest=new hotel_guest();
				if(hg == true) {
					guest=HG.queryHotel_Guest(Id);
			        HttpSession se = request.getSession();
			        se.setAttribute("guest",guest);
			        RequestDispatcher rd=request.getRequestDispatcher("Hotel_All");
			    	rd.forward(request, response);
			    }
				else {
					response.sendRedirect("jsp/login.jsp?err=-1");	
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
