package cn.control;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.dao.hotel_guestDao;
import cn.dao.hotel_roomDao;
import cn.pojo.hotel_guest;
import cn.pojo.hotel_room;


public class GuestRegister extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		hotel_guest  hg=new hotel_guest();
		hg.setHG_name(req.getParameter("HG_name"));
		hg.setHG_sex(req.getParameter("HG_sex"));
		hg.setHG_passwd(req.getParameter("HG_passwd"));
		hg.setHG_phone(req.getParameter("HG_phone"));
		hg.setHG_exmail(req.getParameter("HG_exmail"));
		hg.setHG_idcard(req.getParameter("HG_idcard"));
 		hotel_guestDao HG=new hotel_guestDao();
 		boolean flag=false;
 		try {
 			flag=HG.addHotel_Guest(hg);
 		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (flag == true) {
			RequestDispatcher rd = req.getRequestDispatcher("jsp/login.jsp");
			rd.forward(req, resp);
 		}
		else {
			resp.sendRedirect("jsp/register.jsp");
		}
	
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

}
