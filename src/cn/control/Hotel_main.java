package cn.control;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.dao.hotel_baseDao;
import cn.pojo.hotel_base;

@SuppressWarnings("serial")
public class Hotel_main extends HttpServlet{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		String id=(String) session.getAttribute("HU_Id");
		hotel_base base=new hotel_base();
		hotel_baseDao basedao= new hotel_baseDao();
		try {
			base=basedao.queryHotel_Base(id);
			req.setAttribute("base", base);
			RequestDispatcher rd=req.getRequestDispatcher("jsp/hotel/hotel_message.jsp");
	    	rd.forward(req, resp);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

}
