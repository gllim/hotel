package cn.control;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.dao.hotel_indentDao;
import cn.pojo.hotel_indent;

public class check_Out extends HttpServlet{  //显示退房订单信息
     protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
    	 hotel_indent check=new hotel_indent();
    	 hotel_indentDao queryHotel=new hotel_indentDao();
    /*	  String HI_checkinDate = request.getParameter("HI_checkinDate");
    	  SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	  java.util.Date date = null;
		try {
			date = formatter.parse(HI_checkinDate);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	  java.sql.Date HI_checkinDate1 = new java.sql.Date(date.getTime()); 
    	  
    	  String HI_departureDate = request.getParameter("HI_departureDate");
    	  SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	  java.util.Date date1 = null;
		try {
			date1 = formatter1.parse(HI_departureDate);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	  java.sql.Date HI_departureDate1 = new java.sql.Date(date1.getTime());
  */  
    	 
    	 check.setHI_guestName(request.getParameter("HI_guestName"));//用reque从表单拿到信息存在
    	 check.setHI_guestPhone(request.getParameter("HI_guestPhone"));
    	 check.setHI_guestIdCard(request.getParameter("HI_guestIdCard"));
      // check.setHI_roomId(Integer.parseInt(request.getParameter("HI_roomId")));
      // check.setHI_prise(Double.parseDouble(request.getParameter("HI_prise")));
    	 check.setHI_statuse(request.getParameter("HI_statuse"));
    	 check.setHI_guestPhone(request.getParameter("HI_guestPhone"));    	
      // check.setHI_indentNumber(Integer.parseInt(request.getParameter("HI_indentNumber")));
      // check.setHI_userPhone(Integer.parseInt(request.getParameter("HI_userPhone")));
    	 check.setHI_hotelName(request.getParameter("HI_hotelName"));
    	 
    	 ArrayList<hotel_indent> list=new  ArrayList<hotel_indent>();
    	 try {
			list = queryHotel.queryHotel_Indent_indentNumber("check_Out");
		     } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		      }
    	 if(list!=null) {
    	 RequestDispatcher rd= request.getRequestDispatcher("/jsp/guest/hotel_guest_right.jsp");
         
         rd.forward(request, response);
         }else{
        	    
         	RequestDispatcher rd= request.getRequestDispatcher("BookJS.jsp");
             
             rd.forward(request, response);
         }
     }
     
     protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	 doGet(request, response);
     }
}
