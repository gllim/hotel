package cn.control;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import cn.dao.hotel_roomDao;
import cn.pojo.hotel_room;

public class RoomAdd extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<String> qqq=new ArrayList<String>();
		List<FileItem> Files = new ArrayList<FileItem>();//存取上传文件 
	    DiskFileItemFactory fu = new DiskFileItemFactory ();//创建一个解析器工厂
	    ServletFileUpload upload = new ServletFileUpload(fu);
		upload.setHeaderEncoding("utf-8");
		byte[] buffer = null;
		try {
			List<FileItem> list = upload.parseRequest(req);//取得表单的数据内容
			for(FileItem items:list){
				if(items.isFormField()){
					qqq.add(items.getString("utf-8"));
				}else {
					Files.add(items);
				}
			}
		for(int i=0;i<Files.size();i++){
			FileItem item = (FileItem)Files.get(i);
			InputStream file = item.getInputStream(); 
			buffer = new byte[file.available()];
			file.read(buffer);
			}
		}
		catch (FileUploadException e1) {
			System.out.println(e1);
		}
		hotel_room addroom =new hotel_room();
		addroom.setHR_id(Integer.parseInt(qqq.get(0)));
		addroom.setHR_name(qqq.get(1));
		addroom.setHR_area(Integer.parseInt(qqq.get(2)));
		addroom.setHR_bedType(qqq.get(3));
		addroom.setHR_checkinNumber(Integer.parseInt(qqq.get(4)));
		addroom.setHR_floor(Integer.parseInt(qqq.get(5)));
		addroom.setHR_window(qqq.get(6));
		addroom.setHR_prise(Double.parseDouble(qqq.get(7)));
		addroom.setHR_photo(buffer);
		HttpSession session = req.getSession();
		String id=(String) session.getAttribute("HU_Id");
		addroom.setHR_hotelname(id);
		hotel_roomDao adddao =new hotel_roomDao();
		boolean add=false;
		try {
			add=adddao.addHotel_Room(addroom);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		resp.sendRedirect("RoomAll");
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(req, resp);
	}
}
