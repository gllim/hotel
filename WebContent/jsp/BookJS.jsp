<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>订房订单</title>
<script src="${pageContext.request.contextPath}/js/register.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/My97DatePicker/WdatePicker.js"></script>
<link href="${pageContext.request.contextPath}/css/BookJS.css" rel="stylesheet" type="text/css" />
</head>
<body>
<%@include file="header.jsp" %>
<div class="center">
<div class="left">
	<h3>填写您的订单</h3>
	<form  method="post" action="${pageContext.request.contextPath}/indentAdd?HI_indentNumber=${requestScope.HI_indentNumber}&HI_roomId=${requestScope.room.HR_id}&HI_hotelName=${requestScope.hotel.HB_id}&HI_userPhone=${sessionScope.guest.HG_phone}">
		<h4><font color="orange">01</font> &nbsp;&nbsp;预定信息</h4>
		<hr color= "#dbeae8"/>
		<table>
			<tr>
				<td class="left1">入住时间</td>
				<td> <input id="qwe1" class="txt" name="HI_checkinDate" type="text" class="Wdate form-controls" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:true});">
						&nbsp;&nbsp;至 &nbsp;&nbsp;
					<input id="asd1" onchange="xxx()" class="txt" name="HI_departureDate" type="text"class="Wdate form-controls" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:true});">&nbsp;&nbsp;共<span  style='color:#FF0000' id="qru"></span>天</td>				
			</tr>
			<tr>
				<td class="left1">房间号码</td>
				<td>${requestScope.room.HR_id}</td>
			</tr>
			<tr>
				<td class="left1">房间总计</td>
				<td id="prise" class="money">${requestScope.room.HR_prise}</td>
			</tr>
		</table>
		<h4><font color="orange">02</font> &nbsp;&nbsp;入住人信息</h4>
		<hr color= "#dbeae8"/>
		<table>
			<tr>
				<td class="left1">入住人姓名</td>
				<td><input type="text" name="HI_guestName" class="txt"></td>
				<td class="info"><font color="red"> *</font>填写姓名必须与入住时所持有效证件姓名保持一致</td>
			</tr>
			<tr>
				<td class="left1">入住人手机</td>
				<td><input type="text" name="HI_guestPhone" class="txt" id="number" onChange="phone()"></td>
				<td class="info"> <font color="red"> *</font>订单提交后，我们会将预订信息发送至您的个人中心</td>
			</tr>
			
			<tr>
			<td class="left1"></td>
			<td class="infer"  id="pp"></td>
			<td></td>
			</tr>
			
			<tr>
				<td class="left1">入住人身份证</td>
				<td><input type="text" name="HI_guestIdCard" class="txt" id="Code" onChange="IdCodeValid()"></td>
				<td></td>
			</tr>
			<tr>
			<td class="left1"></td>
			<td class="infer"  id="idcard"></td>
			<td></td>
			</tr>
			
		</table>
		<h4><font color="orange">03</font> &nbsp;&nbsp;订单信息</h4>
		<hr color= "#dbeae8"/>
		<table>
			<tr>
				<td class="left1">订单编号</td>
				<td>${requestScope.HI_indentNumber}</td>
			</tr>
			<tr>
				<td class="left1">订单酒店</td>
				<td>${requestScope.hotel.HB_id}</td>
			</tr>
			<tr>
				<td class="left1">订单状态</td>
				<td>待支付</td>
			</tr>
		</table>
		<h4><font color="orange">04</font> &nbsp;&nbsp;房费信息</h4>
		<hr color= "#dbeae8"/>
		<div class="room">
			<table class="room1">
				<tr>
					<td >房费总计￥</td>
					<td><input  class="txt1" readonly="readonly" value="" type="text"></td>
				</tr>
				<tr>
					<td >实际应付￥</td>
					<td ><input name="HI_prise" id="HI_prise" class="txt2" readonly="readonly" value="338" type="text"></td>
				</tr>
			</table>
			<input type="submit" class="radio" value="提交">
		</div>
	</form>
</div>
<div class="right">
	<h2>${requestScope.hotel.HB_id}</h2><br/>
	<p>地址：&nbsp;&nbsp;${requestScope.hotel.HB_url}</p><br/>
	<p>电话：&nbsp;&nbsp;${requestScope.hotel.HB_phone}</p>
	<br/><hr/>
	<form action="">
		<table class="table_right">
		<tr>
				<td class="top">${requestScope.room.HR_name}</td>
				<td></td>
			</tr>
			<tr>
				<td>床型：${requestScope.room.HR_bedType}</td>
				<td>面积：${requestScope.room.HR_area}</td>
			</tr>
			<tr>
				<td>窗户：${requestScope.room.HR_window}</td>
				<td>楼层：${requestScope.room.HR_floor}层</td>
			</tr>
			<tr>
				<td>入住人数：${requestScope.room.HR_checkinNumber}</td>
				<td>是否无烟：无限制</td>
			</tr><tr>
				<td>宽带上网：客房WIFI</td>
				<td></td>
			</tr>
		</table>
	</form>
	<br/>
	<h2>订单政策</h2><br/>
	<p>1、请于入住日12:00后办理入住，如需提前入住，请联系酒店前台确认；</p>
	<p>2、预付房费订单房间将为您整晚保留；</p>
	<p>3、预付房费后，如需取消订单/申请退款，请在入住当日18:00前联系酒店前台确认。</p>
	<p>4、秒杀价/首住7折/积分兑换房，预订成功后，均不可取消订单和退款；</p>
	<p>5、会员价/早订早惠/预付立减20元/立减房/买赠促，均限时取消：</p>
	<p>a、未入住订单，预订3间及以下，入住当日18：00前可免费取消全额退款，入住当日18：00后取消需扣除所预订房间的全部首晚房费；</p>
	<p>b、未入住订单，预订3间以上，入住前一日18：00前可免费取消全额退款，入住前一日18：00后取消需扣除所预订房间的全部首晚房费；</p>
	<p>c、已入住订单，无法取消订单和退款，如有变动，请联系酒店沟通；</p>
	
</div>
</div>
<%@include file="footer.jsp" %>
</body>
</html>