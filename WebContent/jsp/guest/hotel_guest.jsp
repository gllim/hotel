<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link href="${pageContext.request.contextPath}/css/hotel_guest.css" rel="stylesheet" type="text/css" />
<title>个人信息显示</title>
</head>
<body>
<%@include file="Rheader.jsp" %>
<div class="menu">
		<ul><li class="rleft"><a href="${pageContext.request.contextPath}/jsp/guest/own.jsp" target="frame">个人中心</a></li>
			<li><a href="${pageContext.request.contextPath}/jsp/guest/hotel_guest_right.jsp" target="frame">我的订单</a></li>
			<li><a href="${pageContext.request.contextPath}/jsp/guest/evaluate.jsp" target="frame">评语</a></li>
			<li><a href="${pageContext.request.contextPath}/Hotel_All">返回首页</a></li>
		</ul>
</div> 
<div class="main">
    	<div class="left">
       		<form action="">
   				<table>
   				 <img src="${pageContext.request.contextPath}/images/user.jpg" class="image">
        			 <tr>
        			 	<td class="tb_left"> 姓&nbsp;&nbsp;&nbsp;&nbsp;名: </td>
        			 	<td>${requestScope.guest.HG_name}</td>
        			 </tr>
        			 <tr>
        			 	<td class="tb_left"> 手机号: </td>
        			 	<td>${requestScope.guest.HG_phone}</td>
        			 </tr><tr>
        			 	<td class="tb_left"> 邮&nbsp;&nbsp;&nbsp;&nbsp;箱: </td>
        			 	<td> ${requestScope.guest.HG_exmail}</td>
        			 </tr>
         		</table>
   			</form>
     	</div>
     	<div class="right">
     		<iframe src="${pageContext.request.contextPath}/Guest_evt?phone=${sessionScope.guest.HG_phone}" frameborder="0" name="frame" width="100%" height="500px" scrolling="auto"></iframe>
 	 	</div>
</div>
</body>
</html>