<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Insert title here</title>
<link href="${pageContext.request.contextPath}/css/hotel_room.css" rel="stylesheet" type="text/css">
</head>
<body>

<table class="main" >
<tr><th>酒店名字</th><th>当前状态</th><th>更改<th>操作</th></tr>
</table>


<c:if test="${!empty requestScope.hotel_user}">
	<c:forEach items="${requestScope.hotel_user}" var="allCourse">
<form method="post" action="${pageContext.request.contextPath}/changeStatuse?ID=${allCourse.HU_Id}">
<table class="main" >
<tr>
<td>${allCourse.HU_Id}</td><td>${allCourse.HU_statuse}</td>
<td><select name="statuse"><option value="通过">通过</option><option value="未通过">未通过</option></select></td>
<td><input type="submit" value="修改"></td>
</tr>
</table>
</form>
	</c:forEach>
</c:if>

</body>
</html>