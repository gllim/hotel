<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>房间信息</title>
<link href="${pageContext.request.contextPath}/css/Room.css" rel="stylesheet" type="text/css" />
</head>
<body>
<%@ include file="header.jsp" %>
<div class="center">
	<div class="room_index">
	<div style="float: left">
		<img class="images_hotel" src="${pageContext.request.contextPath}/displayImg?qqq=hotel_base&type=HB_id&text=${requestScope.base.HB_id}&ss=HB_photo">
	</div>
	<div class="z">
		<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${requestScope.base.HB_profile}<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${requestScope.base.HB_id}，诚心恭候您的光临！
		</p>
	</div>
	</div>
	<hr />
	<form action="${pageContext.request.contextPath}/Hotel_All">
		<table class="room_type">
			<tr>
				<td>床型</td>
				<td><select class="select">
						<option>大床</option>
						<option>双床</option>
					</select></td>
				<td>&nbsp;&nbsp;窗户</td>
				<td><select class="select">
						<option>有</option>
						<option>无</option>
					</select></td>
				<td>&nbsp;&nbsp;楼层</td>
				<td><select class="select">
						<option>低楼层</option>
						<option>中楼层</option>
						<option>高楼层</option>
					</select></td>
				<td>&nbsp;&nbsp;入住人数</td>
				<td><select class="select">
						<option>1</option>
						<option>2</option>
						<option>3</option>
					</select></td>
				<td>&nbsp;&nbsp;<input type="text" class="select_key" placeholder="请输入关键字" autocomplete="off"data-component="search/destination/input-placeholder"></td>
				<td>&nbsp;&nbsp;<input type="submit" value="搜索" class="select_submit"></td>
			</tr>
		</table>
	</form>
	<c:forEach items="${requestScope.list}" var="list">
	<div class="room">
	
		<form method="post" action="${pageContext.request.contextPath}/indent?RoomID=${list.HR_id}&hotelName=${requestScope.hotelName}" >
			<div class="image"><img class="room_img" src="${pageContext.request.contextPath}/displayImg?qqq=hotel_room&type=HR_id&text=${list.HR_id}&ss=HR_photo"></div>
			<div class="room_center">
				<h3>${list.HR_name}</h3><br />
				<table>
					<tr>
						<td>床型：${list.HR_bedType}</td>
						<td>面积：${list.HR_area}㎡</td>
					</tr>
					<tr>
						<td>窗户：${list.HR_window}</td>
						<td>楼层：${list.HR_floor}层</td>
					</tr>
					<tr>
						<td>是否无烟：无限制</td>
						<td></td>
					</tr>
					<tr>
						<td>宽带上网：客房WIFI</td>
						<td></td>
					</tr>
				</table>
			</div>
			<div class="room_right">
				<h2><font size="5px">￥</font>${list.HR_prise}</h2>
				<input type="submit" value="预订"class="room_radio">
			</div>
		</form>
	
	</div>
	</c:forEach>
</div>
<%@include file="footer.jsp" %>
</body>
</html>