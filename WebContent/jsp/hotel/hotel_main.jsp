<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>酒店管理</title>
<link href="${pageContext.request.contextPath}/css/hotel.css" rel="stylesheet" type="text/css">
<script src="${pageContext.request.contextPath}/js/hotel_main.js" type="text/javascript"></script>
</head>
<body>
<div id="left">
<p id="p">当前登陆用户：</p><p id="qqq">${sessionScope.HU_Id}</p>
<ul>
  <li><a name="t" onclick="qq(this)" class="active" href="${pageContext.request.contextPath}/Hotel_main" target="right1Frame">查看信息</a></li>
  <li><a name="t" onclick="qq(this)" href="${pageContext.request.contextPath}/Hotel_baseUpdate" target="right1Frame">修改信息</a></li>
  <li><a name="t" onclick="qq(this)" href="${pageContext.request.contextPath}/RoomAll" target="right1Frame">查看房间</a></li>
  <li><a name="t" onclick="qq(this)" href="${pageContext.request.contextPath}/jsp/hotel/hotel_addRoom.jsp" target="right1Frame">添加房间</a></li>
  <li><a name="t" onclick="qq(this)" href="${pageContext.request.contextPath}/reservation" target="right1Frame">查看预定</a></li>
</ul>
</div>
<div id="right">
<iframe name="right1Frame" src="${pageContext.request.contextPath}/Hotel_main" id="right1Frame" height="100%" scrolling="auto" frameborder="0" width="100%"></iframe>
</div>

</body>
</html>