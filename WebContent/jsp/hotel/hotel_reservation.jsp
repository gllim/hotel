<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Insert title here</title>
<link href="${pageContext.request.contextPath}/css/hotel_indent.css" rel="stylesheet" type="text/css">
</head>
<body>
<table class="main"  cellpadding="6">
<tr>
<th>订单编号</th>
<th>用户ID</th>
<th>房间ID</th>
<th>预订人姓名</th>
<th>预订人电话</th>
<th>预订人身份证</th>
<th>入住日期</th>
<th>退房日期</th>
<th>预订的房间价格</th>
<th>订单状态</th>
<th>操作</th>
</tr>
<c:forEach  items="${requestScope.indent}" var="reserve">
<tr onmouseover="this.style.backgroundColor='#96A6A9';" onmouseout="this.style.backgroundColor='#d4e3e5';">
<td>${reserve.HI_indentNumber }</td>
<td>${reserve.HI_userPhone}</td>
<td>${reserve.HI_roomId }</td>
<td>${reserve.HI_guestName }</td>
<td>${reserve.HI_guestPhone }</td>
<td>${reserve.HI_guestIdCard }</td>
<td>${reserve.HI_checkinDate }</td>
<td>${reserve.HI_departureDate }</td>
<td>${reserve.HI_prise }</td>
<td>${reserve.HI_statuse }</td>
<td><a href="${pageContext.request.contextPath}/changeRe?type=pass&HI_indentNumber=${reserve.HI_indentNumber}"><img src="${pageContext.request.contextPath}/images/table_(add)_16x16.gif"></a>
	<a href="${pageContext.request.contextPath}/changeRe?type=aa&HI_indentNumber=${reserve.HI_indentNumber}"><img src="${pageContext.request.contextPath}/images/table_(delete)_16x16.gif"></a></td>
</tr>
</c:forEach>
</table>



</body>
</html>