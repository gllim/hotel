<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Insert title here</title>
<link href="${pageContext.request.contextPath}/css/hotel_room.css" rel="stylesheet" type="text/css">
</head>
<body>
<table class="main"  cellpadding="6">
<tr>
<th>房间ID</th>
<th>房间类型</th>
<th>房间面积</th>
<th>房间床型</th>
<th>房间入住人数</th>
<th>房间楼层</th>
<th>房间窗户</th>
<th>房间价格</th>
<th>房间照片</th>
<th>操作</th>
</tr>
<c:forEach items="${requestScope.allroom}" var="allroom">
<tr onmouseover="this.style.backgroundColor='#96A6A9';" onmouseout="this.style.backgroundColor='#d4e3e5';">
<td>${allroom.HR_id}</td>
<td>${allroom.HR_name}</td>
<td>${allroom.HR_area}</td>
<td>${allroom.HR_bedType}</td>
<td>${allroom.HR_checkinNumber}</td>
<td>${allroom.HR_floor}</td>
<td>${allroom.HR_window}</td>
<td>${allroom.HR_prise}</td>
<td><img width="20px" height="20px" src="${pageContext.request.contextPath}/displayImg?qqq=hotel_room&type=HR_id&text=${allroom.HR_id}&ss=HR_photo"></td>
<td><a href="${pageContext.request.contextPath}/RoomUpdate?HR_id=${allroom.HR_id}"><img src="${pageContext.request.contextPath}/images/record_(edit)_16x16.gif" class="images_hotel"></a>
	<a href="${pageContext.request.contextPath}/RoomDelete?HR_id=${allroom.HR_id}"><img src="${pageContext.request.contextPath}/images/record_(delete)_16x16.gif" class="images_hotel"></a></td>
</tr>
</c:forEach>
</table>



</body>
</html>